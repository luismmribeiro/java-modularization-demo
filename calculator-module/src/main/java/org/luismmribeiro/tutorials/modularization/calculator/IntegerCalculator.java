package org.luismmribeiro.tutorials.modularization.calculator;

/**
 * The integer calculator API interface
 * 
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
public interface IntegerCalculator extends Calculator<Integer> {

}
