package org.luismmribeiro.tutorials.modularization.other.calculator;

import org.luismmribeiro.tutorials.modularization.calculator.Calculator;

/**
 * Double values calculator.
 * 
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
public class DoubleCalculator implements Calculator<Double> {
    /** {@inheritDoc} */
    @Override
    public Double sum(Double a, Double b) {
        return a + b;
    }

    /** {@inheritDoc} */
    @Override
    public Double subtraction(Double a, Double b) {
        return a - b;
    }

    /** {@inheritDoc} */
    @Override
    public Double multiplication(Double a, Double b) {
        return a * b;
    }

    /** {@inheritDoc} */
    @Override
    public Double division(Double dividend, Double divisor) {
        return dividend / divisor;
    }
}
