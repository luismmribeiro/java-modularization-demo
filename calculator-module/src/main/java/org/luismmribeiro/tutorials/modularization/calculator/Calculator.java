package org.luismmribeiro.tutorials.modularization.calculator;

/**
 * The calculator API interface
 * 
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
public interface Calculator<T extends Number> {
    /**
     * Performs the sum operation between two Numbers.
     * 
     * @param a
     * @param b
     * @return a + b
     */
    T sum(T a, T b);

    /**
     * Performs the subtraction operation between two Numbers.
     * 
     * @param a
     * @param b
     * @return a - b
     */
    T subtraction(T a, T b);

    /**
     * Performs the product between two Numbers.
     * 
     * @param a
     * @param b
     * @return a * b
     */
    T multiplication(T a, T b);

    /**
     * Performs the division between two Numbers.
     * 
     * @param dividend the number being divided
     * @param divisor a number that will divide the dividend exactly
     * @return dividend / divisor
     */
    T division(T dividend, T divisor);
}
