module myCalculator {
    exports org.luismmribeiro.tutorials.modularization.calculator to myCalculator.consumer;
    provides org.luismmribeiro.tutorials.modularization.calculator.IntegerCalculator with 
        org.luismmribeiro.tutorials.modularization.calculator.IntegerCalculatorImpl;
}
