# java-modularization-demo

This project was created to provide a small tutorial for the new Java modularization feature.

The code was developed by [Luis Ribeiro](mailto:luismmribeiro@gmail.com) and it provides no warranties what so ever.

In order to correctly execute and run this code, you need to have OpenJDK 13+ and maven 3.6.0+ installed on your workstation.