package org.luismmribeiro.tutorials.modularization.calculator.consumer;

import org.luismmribeiro.tutorials.modularization.calculator.IntegerCalculator;
import org.luismmribeiro.tutorials.modularization.calculator.IntegerCalculatorImpl;

/**
 * The consumer of myCalculator module
 * 
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
public class CalculatorConsumer {
    public static void main(String[] args) throws Exception {
        IntegerCalculator integerCalculator = new IntegerCalculatorImpl();
        
        System.out.println("The result is: " + integerCalculator.sum(5, 4));
        
        // The class DoubleCalculator is also in myCalculator module. But, as the 
        // module does not expose its package, it can't be accessed nor used by other
        // modules.
        //Calculator<Double> doubleCalculator = new DoubleCalculator();
    }
}
