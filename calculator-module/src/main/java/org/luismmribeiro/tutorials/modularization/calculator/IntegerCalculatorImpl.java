package org.luismmribeiro.tutorials.modularization.calculator;

/**
 * Integer values calculator.
 * 
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
public class IntegerCalculatorImpl implements IntegerCalculator {
    /** {@inheritDoc} */
    @Override
    public Integer sum(Integer a, Integer b) {
        return a + b;
    }

    /** {@inheritDoc} */
    @Override
    public Integer subtraction(Integer a, Integer b) {
        return a - b;
    }

    /** {@inheritDoc} */
    @Override
    public Integer multiplication(Integer a, Integer b) {
        return a * b;
    }

    /** {@inheritDoc} */
    @Override
    public Integer division(Integer dividend, Integer divisor) {
        return dividend / divisor;
    }
}
